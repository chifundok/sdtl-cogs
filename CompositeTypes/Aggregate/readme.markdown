An aggregation summarizes data using aggregation functions applied to data that may be grouped
by one or more variables. The resulting summary data is added to each row of the
existing dataset.
