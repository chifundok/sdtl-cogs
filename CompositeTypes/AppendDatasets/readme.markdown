Merges datasets containing different variables for an overlapping set of cases.  The merge is controlled by one or more grouping variables.
