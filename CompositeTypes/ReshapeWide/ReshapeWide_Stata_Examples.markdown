Example 1:
``` 
reshape wide inc ue, i(region id) j(year)
 
{"command": "ReshapeWide",
    "MakeItems": [
            "ReshapeItemDescription":
                {"$type": "ReshapeItemDescription",
                "SourceVariableName": "inc",
                "Stub": "inc",
				"IndexVariableName":"year",
                "IndexValues": 
                {"$type":"NumberRangeExpression",
                        "From": "80",
                        "To": "83",
                        "By": "1"}
                    },
            "ReshapeItemDescription":
                {"$type": "ReshapeItemDescription",
                "SourceVariableName": "ue",
                "Stub": "ue",
                "IndexValues": 
                {"$type":"NumberRangeExpression",
                        "From": "80",
                        "To": "83",
                        "By": "1"}
                    }
            ],
    "IDVariables": [ 
                {"$type": "VariableSymbolExpression","VariableName": "region"},
                {"$type": "VariableSymbolExpression","VariableName": "id"}
                ],
        }
```        
    
	   
	   
******************************     
Example 2:
```
{"command":"ReshapeWide",
	"MakeItems":[
		"ReshapeItemDescription":
		{"SourceVariableName":"Records",
			"Stub":"",
			"IndexVariableName":"Status",
			"IndexValues":
			{"$type":"StringListExpression",
				"values":[ "illegal", "legal"]
				}
				}
			],
	"IDVariables":[
		{"$type": "VariableSymbolExpression","VariableName":"Centre"},
		{"$type": "VariableSymbolExpression","VariableName":"MetricTable"},
		{"$type": "VariableSymbolExpression","VariableName":"QMetric"}
		]
	}

```
		
		
